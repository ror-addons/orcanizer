-- Warhammer Online Ingame-Orcanizer
-- Version 1.3
-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- Noteable contributions:
-- - The French replacements/rules were contributed by the guild Soleil Noir, Har Ganeth
-- - pyroxide provided many additions to the English rule set

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

-- orcanizer namespace
orcanizer = {}

-- stores the replaced "on chat submit" callback
local fnOnChatFunction = nil
-- stores the current language's orcanize function
local fnOrcanizeFunction = nil

-- localizable strings
local sOrcanizerEnabled
local sOrcanizerDisabled
local sOrcanizerSyntax
local sOrcanizerHardcore
local sOrcanizerContextCaption
local sOrcanizerContextEnable
local sOrcanizerContextDisable
local sOrcanizerContextHardcore

local sOrcanizerContextHookWindow = "EA_TextEntryGroupChannelButton"

local bUseOrcanizer

function orcanizer.OnInitialize()
  -- hook enter key presses in chat window
  fnOnChatFunction = EA_ChatWindow.OnKeyEnter
  EA_ChatWindow.OnKeyEnter = orcanizer.OnChatKeyEnter
  
  -- event for context menu
  WindowRegisterCoreEventHandler(sOrcanizerContextHookWindow, "OnRButtonUp", "orcanizer.OnContextMenu")
  
  -- default orcanizer to be enabled
  if bOrcanizerEnabled == nil then
    bOrcanizerEnabled = true
  end
  if bOrcanizerHardcore == nil then
    bOrcanizerHardcore = false
  end

  -- use orcanizer if hardcore mode is enabled or player's race is a greenskin
  bUseOrcanizer = (bOrcanizerEnabled and (bOrcanizerHardcore == true or GameData.Player.race.id == GameData.Races.ORC or GameData.Player.race.id == GameData.Races.GOBLIN))
  
  orcanizer.UpdateChatInput()
  
  -- select a language, set up the right orcanize function and localized strings
  if SystemData.Settings.Language.active == SystemData.Settings.Language.GERMAN then
    -- german
    sOrcanizerEnabled = L"Der Orcanizer wurde aktiviert."
    sOrcanizerDisabled = L"Der Orcanizer wurde deaktiviert."
    sOrcanizerSyntax = L"Verwendung: /orc {on|off|hc}"
	sOrcanizerHardcore = L"Der Orcanizer wurde aktiviert. [Hardcore-Modus]"
	sOrcanizerContextCaption = L"Orcanizer-Einstellungen"
	sOrcanizerContextEnable = L"Aktivieren (Standard)"
	sOrcanizerContextDisable = L"Deaktivieren"
	sOrcanizerContextHardcore = L"Aktivieren (Hardcore)"
    fnOrcanizeFunction = orcanizer.OrcanizeDE
  else
    if SystemData.Settings.Language.active == SystemData.Settings.Language.FRENCH then
	  -- french
      sOrcanizerEnabled = L"L'orkanizeur a �t� aktiv�.\nGloire au Soleil Noir, Har Ganeth."
      sOrcanizerDisabled = L"L'orkanizeur a �t� d�zaktiv�.\nVersion Gloire au Soleil Noir, Har Ganeth."
	  sOrcanizerHardcore = L"L'orkanizeur a �t� aktiv�. [hardcore]\nGloire au Soleil Noir, Har Ganeth."
      sOrcanizerSyntax = L"Usage: /orc {on|off|hc}"
	  sOrcanizerContextCaption = L"Orkanizeur Settings"
	  sOrcanizerContextEnable = L"Activate (Standard)"
	  sOrcanizerContextDisable = L"Deactivate"
	  sOrcanizerContextHardcore = L"Activate (Hardcore)"
	  fnOrcanizeFunction = orcanizer.OrcanizeFR
	else
      if SystemData.Settings.Language.active == SystemData.Settings.Language.ITALIAN then
	    -- italian
        sOrcanizerEnabled = L"The Orcanizer has been activated.\nSorry, but the Italian version of the Orcanizer is not yet completed."
        sOrcanizerDisabled = L"The Orcanizer has been deactivated.\nSorry, but the Italian version of the Orcanizer is not yet completed."
        sOrcanizerSyntax = L"Usage: /orc {on|off|hc}\nSorry, but the Italian version of the Orcanizer is not yet completed."
		sOrcanizerHardcore = L"The Orcanizer has been activated. [hardcore mode]\nSorry, but the Italian version of the Orcanizer is not yet completed."
		sOrcanizerContextCaption = L"Orcanizer Settings"
	    sOrcanizerContextEnable = L"Activate (Standard)"
	    sOrcanizerContextDisable = L"Deactivate"
	    sOrcanizerContextHardcore = L"Activate (Hardcore)"
	    fnOrcanizeFunction = orcanizer.OrcanizeIT
	  else
	    -- english (default)
        sOrcanizerEnabled = L"The Orcanizer has been activated."
        sOrcanizerDisabled = L"The Orcanizer has been deactivated."
        sOrcanizerSyntax = L"Usage: /orc {on|off|hc}"
		sOrcanizerHardcore = L"The Orcanizer has been activated. [hardcore mode]"
		sOrcanizerContextCaption = L"Orcanizer Settings"
	    sOrcanizerContextEnable = L"Activate (Standard)"
	    sOrcanizerContextDisable = L"Deactivate"
	    sOrcanizerContextHardcore = L"Activate (Hardcore)"
	    fnOrcanizeFunction = orcanizer.OrcanizeEN
	  end
	end
  end
end

function orcanizer.OnShutdown()
  -- disabled for the moment - unsafe right now

  -- restore original event handler
  -- EA_ChatWindow.OnKeyEnter = fnOnChatFunction
end

function orcanizer.OnContextMenu()
  -- Shows a simple context menu for quick configuration
  EA_Window_ContextMenu.CreateContextMenu(sOrcanizerContextHookWindow,nil,sOrcanizerContextCaption)
  EA_Window_ContextMenu.AddMenuItem(sOrcanizerContextEnable,orcanizer.Enable,bOrcanizerEnabled and not bOrcanizerHardcore,true);
  EA_Window_ContextMenu.AddMenuItem(sOrcanizerContextHardcore,orcanizer.Hardcore,bOrcanizerEnabled and bOrcanizerHardcore,true);
  EA_Window_ContextMenu.AddMenuItem(sOrcanizerContextDisable,orcanizer.Disable,not bOrcanizerEnabled,true);
  EA_Window_ContextMenu.Finalize();
end

function orcanizer.Enable()
	  bOrcanizerEnabled = true
	  bOrcanizerHardcore = false
	  bUseOrcanizer = (bOrcanizerEnabled and (bOrcanizerHardcore == true or GameData.Player.race.id == GameData.Races.ORC or GameData.Player.race.id == GameData.Races.GOBLIN))
	  orcanizer.UpdateChatInput()
	  EA_ChatWindow.Print(sOrcanizerEnabled)
end

function orcanizer.Disable()
	  bOrcanizerEnabled = false
	  bOrcanizerHardcore = false
	  bUseOrcanizer = (bOrcanizerEnabled and (bOrcanizerHardcore == true or GameData.Player.race.id == GameData.Races.ORC or GameData.Player.race.id == GameData.Races.GOBLIN))
	  orcanizer.UpdateChatInput()
	  EA_ChatWindow.Print(sOrcanizerDisabled)
end

function orcanizer.Hardcore()
	  bOrcanizerEnabled = true
	  bOrcanizerHardcore = true
	  bUseOrcanizer = (bOrcanizerEnabled and (bOrcanizerHardcore == true or GameData.Player.race.id == GameData.Races.ORC or GameData.Player.race.id == GameData.Races.GOBLIN))
	  orcanizer.UpdateChatInput()
      EA_ChatWindow.Print(sOrcanizerHardcore)
end

function orcanizer.OnChatKeyEnter(...)
  -- get written text from chat input
  sText = EA_TextEntryGroupEntryBoxTextInput.Text
  
  -- skip everything for empty input
  if (sText == nil) or (sText == L"") then
    return fnOnChatFunction(...)
  end
  
  -- get first character (difference between command and text)
  sFirst = WStringToString(sText):sub(1,1)
  
  -- check if a command has been entered 
  if sFirst == "]" or sFirst == "*" then
    -- do nothing
  else if sFirst == "/" then
    -- split command and parameter(s)
	sCommand, sParameters = sText:match(L"/([A-Za-z0-9]+) *(.*)")
	
	if sCommand == nil then
	  sCommand = L""
	end
	if sParameters == nil then
	  sParameters = L""
	end
	
	-- /orc is handled by this addon
	if sCommand == L"orc" then
	  -- enable orcanizer
	  if sParameters == L"on" then
	    orcanizer.Enable()
	  -- disable orcanizer
	  else
	    if sParameters == L"off" then
	      orcanizer.Disable()
	    else
		  if sParameters == L"hc" then
		    orcanizer.Hardcore()
		  else
            -- unknown parameter supplied
            EA_ChatWindow.Print(sOrcanizerSyntax)
		  end
		end
	  end

      -- use orcanizer if hardcore mode is enabled or player's race is a greenskin
	  bUseOrcanizer = (bOrcanizerEnabled and (bOrcanizerHardcore == true or GameData.Player.race.id == GameData.Races.ORC or GameData.Player.race.id == GameData.Races.GOBLIN))
	  orcanizer.UpdateChatInput()
	  -- don't pass the text to other handlers as it's handled already
	  EA_TextEntryGroupEntryBoxTextInput.Text = L""
	end
  else
    -- orcanize the text written if enabled
	if bOrcanizerEnabled == true then
	  -- skip in case the text is empty and/or player isn't a greenskin
	  if sText == "" then
	    
	  else
	    if bUseOrcanizer then
		  -- lots of work in one line:
		  -- * convert to single character string (wstring -> string)
		  -- * append spaces (easier handling)
		  -- * orcanize (language specific)
		  -- * remove additional spaces
		  -- * convert back to wide character string (string -> wstring)
          EA_TextEntryGroupEntryBoxTextInput.Text = towstring(" "..(((fnOrcanizeFunction(" "..(WStringToString(sText)):gsub("([\.,;\-!\?\(\)])","	%1").." ")):gsub("	+([\.,;\-!\?\(\)])","%1")):gsub("^%s+","")):gsub("%s+$",""))
  		end
	  end
	end
  end
  end
  -- continue other/default event handler(s)
  return fnOnChatFunction(...)
end

-- OrcanizeEN() - applies all replacements - English version
function orcanizer.OrcanizeEN(sText)
  -- specific words
  sText = sText:gsub("dwar[fv]es","stunties")
  sText = sText:gsub("dwarf","stunty")
  sText = sText:gsub("Dwar[fv]es","Stunties")
  sText = sText:gsub("Dwarf","Stunty")
  sText = sText:gsub("[Tt]hem(%s)","'em%1")

  sText = sText:gsub("High ?[Ee]lf","Uvva pointy ear")
  sText = sText:gsub("high ?[Ee]lf","uvva pointy ear")
  sText = sText:gsub("(Dark ?)?[Ee]lf","Pointy ear")
  sText = sText:gsub("(dark ?)?[Ee]lf","pointy ear")

  sText = sText:gsub("High ?[Ee]l[fv]es","Uvva pointy earz")
  sText = sText:gsub("high ?[Ee]l[fv]es","uvva pointy earz")
  sText = sText:gsub("(Dark ?)?[Ee]l[fv]es","Pointy earz")
  sText = sText:gsub("(dark ?)?[Ee]l[fv]es","pointy earz")
    
  sText = sText:gsub("([Gg])uy","%1it")
  
  -- pyroxide's rules
  sText = sText:gsub("[Aa]bout","'bout")
  sText = sText:gsub("([Aa])nd","%1n'")
  sText = sText:gsub("[Aa]round","'round")
  sText = sText:gsub("[Aa]rrow","%1rrer")
  sText = sText:gsub("([Bb])est(%A)","%1estest%2")
  sText = sText:gsub("([Cc])haos","%1hangy humie")
  sText = sText:gsub("(%A)crap","%1slop")
  sText = sText:gsub("Crap","Slop")
  sText = sText:gsub("dead(%A)","kilt%1")
  sText = sText:gsub("Dead(%A)","Kilt%1")
  sText = sText:gsub("died(%A)","got kilt%1")
  sText = sText:gsub("Died(%A)","Got kilt%1")
  sText = sText:gsub("(%A)dying","%1gettin' kilt")
  sText = sText:gsub("(%A)Dying","%1Gettin' kilt")
  sText = sText:gsub("([Ee])mpire","%1mpi'a")
  sText = sText:gsub("Enough","A'nuff")
  sText = sText:gsub("enough","a'nuff")
  sText = sText:gsub("([Ee])verything","%1v'ryfing")
  sText = sText:gsub("([Ff])avorite","%1av'rit")
  sText = sText:gsub("([Ff])(l?)avor","%1%2av'r")
  sText = sText:gsub("fight","scrap")
  sText = sText:gsub("Fight","Scrap")
  sText = sText:gsub("(%A[Ff])or(%A)","%1er%2")
  sText = sText:gsub("([GgWw])ood","%1ud")
  sText = sText:gsub("[Hh]ow am","'ow's")
  sText = sText:gsub("[Hh]ow did","'ow'd")
  sText = sText:gsub("[Hh]ow is","'ow's")
  sText = sText:gsub("[Hh]ow's","'ow's")
  sText = sText:gsub("([Hh])uman","%1umie")
  sText = sText:gsub("(%A)[Ii]'m(%A)","%1I's%2")
  sText = sText:gsub("(%A)[Ii] am(%A)","%1I's%2")
  sText = sText:gsub("([Kk])illed","%1ilt")
  sText = sText:gsub("leader","boss")
  sText = sText:gsub("Leader","Boss")
  sText = sText:gsub("([Ll])ittle","%1it'l")
  sText = sText:gsub("(%A[Mm])y(%A)","%1e%2")
  sText = sText:gsub("([Nn])othing","%1uffink")
  sText = sText:gsub("([Oo])ut of(%A)","%1utta%2")
  sText = sText:gsub("([Ss])omething","%1umfink")
  sText = sText:gsub("([Ss])ome","%1um")
  sText = sText:gsub("thank","fank")
  sText = sText:gsub("Thank","Fank")
  sText = sText:gsub("(%A)than","%1den")
  sText = sText:gsub("Than","Den")
  sText = sText:gsub("that","dat")
  sText = sText:gsub("That","Dat")
  sText = sText:gsub("their","der")
  sText = sText:gsub("Their","Der")
  sText = sText:gsub("(%A)then","%1din")
  sText = sText:gsub("Then","Din")
  sText = sText:gsub("(%A)there","%1dere")
  sText = sText:gsub("There","Dere")
  sText = sText:gsub("they're","dey's")
  sText = sText:gsub("They're","Dey's")
  sText = sText:gsub("they are","dey is")
  sText = sText:gsub("They are","Dey is")
  sText = sText:gsub("(%A)thing","%1fing")
  sText = sText:gsub("Thing","Fing")
  sText = sText:gsub("think","fink")
  sText = sText:gsub("Think","Fink")
  sText = sText:gsub("(%A)this(%A)","%1dis%2")
  sText = sText:gsub("This(%A)","Dis%2")
  sText = sText:gsub("those are","dem's")
  sText = sText:gsub("Those are","Dem's")
  sText = sText:gsub("those","dose")
  sText = sText:gsub("Those","Dose")
  sText = sText:gsub("thought","fo't")
  sText = sText:gsub("Thought","Fo't")
  sText = sText:gsub("though(%A)","doe%1")
  sText = sText:gsub("Though(%A)","Doe%1")
  sText = sText:gsub("([Tt])ough","%1uff")
  sText = sText:gsub("(%A[Tt])o(%A)","%1a%2")
  sText = sText:gsub("([Ww])e[' ]a?re","%1e'z")
  sText = sText:gsub("([Ww])hat[' ]i?s [td]h?at","%1hazat")
  sText = sText:gsub("([Ww])hat are","%1ot're")
  sText = sText:gsub("([Ww])hat","%1ot")
  sText = sText:gsub("([Ww])on(%A)","%1ins%2")
  sText = sText:gsub("([Ww])ould","%1ud")
  sText = sText:gsub("([Yy])ou[' ]a?re","%1ouse")
  sText = sText:gsub("([Yy])ourself","%1a'self")
  sText = sText:gsub("([Yy])our","%1er")
  sText = sText:gsub("(%A[Yy])ou(%A)","%1a%2")
  sText = sText:gsub("(%A)are(%A)","%1is%2")
  sText = sText:gsub("Are(%A)","Is%1")
  sText = sText:gsub("(%a+)ing(%A)","%1in'%2")
  -- end of pyroxide's rules
  
  -- articles, etc.
  sText = sText:gsub("([Tt])he(%s)","%1ha%2")

  -- word endings
  sText = sText:gsub("([A-RT-Za-rt-z])s(%s)","%1z%2")
  sText = sText:gsub("ough(%s)","uff%1")
  sText = sText:gsub("([^i])er(%s)","%1a%2")

  -- other
  sText = sText:gsub("(%s)Th","%1D")
  sText = sText:gsub("(%s)th","%1d")
  sText = sText:gsub("th(%s)","f%1")
  sText = sText:gsub("(%s)[Hh](%a)","%1'%2")

  return sText
end

-- OrcanizeFR() - applies all replacements - French version
function orcanizer.OrcanizeFR(sText)
  -- Work in progress 
  
  -- Bidouilles pour faire des exceptions de certains mots.
  sText = sText:gsub("((%s)[Ee])n fait(%s)","%1kkrmpl%2")
  sText = sText:gsub("([Pp])eut[- ]�tre","%1'tet") 
  sText = sText:gsub("([ 'z])([Pp])'tet([ .x])","%1%2krmpl%3")
  sText = sText:gsub("([ 'z])eau([ .x])","%1krmpl%2")
  sText = sText:gsub("([ 'z])Eau([ .x])","%1Krmpl%2")
  sText = sText:gsub("((%s)[Cc])her(%s)","%1kkrmpl%2")
  
  -- specific words
  sText = sText:gsub("Nain","Nabot")
  sText = sText:gsub("Nains","Nabots")
  sText = sText:gsub("nain","nabot")
  sText = sText:gsub("nains","nabots")
  
  sText = sText:gsub("Haut [Ee]lfe","Blondinet grand'zoreille")
  sText = sText:gsub("Hauts [Ee]lfes","Blondinets grand'zoreilles")
  sText = sText:gsub("haut elfe","blondinet grand'zoreille")
  sText = sText:gsub("hauts elfes","blondinets grand'zoreilles")
  sText = sText:gsub("haut elfes","blondinets grand'zoreilles")
  
  sText = sText:gsub("Elfe","Grand'zoreille")
  sText = sText:gsub("Elfes","Grand'zoreilles")
  sText = sText:gsub("elfe","grand'zoreille")
  sText = sText:gsub("elfes","grand'zoreilles")
  
  sText = sText:gsub("Homme","Zom'")
  sText = sText:gsub("Hommes","Zom's")
  sText = sText:gsub("homme","zom'")
  sText = sText:gsub("hommes","zom's")
  sText = sText:gsub("Humain","Zom'")
  sText = sText:gsub("Humains","Zom's")
  sText = sText:gsub("humain","zom'")
  sText = sText:gsub("humain","zom's")

  sText = sText:gsub("Gobelin","Gob'")
  sText = sText:gsub("gobelin","gob'")

  sText = sText:gsub("T�te","Kaboch'")
  sText = sText:gsub("t�te","kaboch'")
  
  sText = sText:gsub("Dent","Ratich'")
  sText = sText:gsub("dent","ratich'")
  
  sText = sText:gsub("(%s)Il est(%s)","%1L'�%2")
  sText = sText:gsub("(%s)il est(%s)","%1l'�%2")
  
  sText = sText:gsub("(%s[Cc])'est(%s)","%1�%2")
  sText = sText:gsub("(%s[Cc])ette(%s)","%1'te%2")
  sText = sText:gsub("(%s[Cc])et(%s)","%1t'")
  sText = sText:gsub("(%s)est(%s)","%1�%2")
  sText = sText:gsub("(%s)�a(%s)","%1sa%2")
  sText = sText:gsub("Quand","Kan")
  sText = sText:gsub("quand","kan")
  
  -- verbes
  sText = sText:gsub("([Jj])e tue(%a)","%1'�klate%2")
  sText = sText:gsub("([Jj])e tue(%s)","%1'�klate%2")
  sText = sText:gsub("(%s)Tue(%a)","%1Eklate%2")
  sText = sText:gsub("(%s)tue(%a)","%1�klate%2")
  sText = sText:gsub("(%s)tua(%a)","%1�klata%2")
  sText = sText:gsub("(%s)Tue(%s)","%1Eklate%2")
  sText = sText:gsub("(%s)tue(%s)","%1�klate%2")
  sText = sText:gsub("(%s)tua(%s)","%1�klata%2")
  
  -- other
  sText = sText:gsub("Qu'est[- ]ce qu'il[ s]","Kesski")
  sText = sText:gsub("qu'est[- ]ce qu'il[ s]","kesski")
  sText = sText:gsub("Qu'est[- ]ce qu'elle[ s]","Kesskel'")
  sText = sText:gsub("qu'est[- ]ce qu'elle[ s]","kesskel'")
  sText = sText:gsub("Qu'est[- ]ce que","Kesske")
  sText = sText:gsub("qu'est[- ]ce que","kesske")
  sText = sText:gsub("Qu'est[- ]ce qu'on","Kesskon")
  sText = sText:gsub("qu'est[- ]ce qu'on","kesskon")
  sText = sText:gsub("qu'ils ont","k'izont")
  sText = sText:gsub("qu'elles ont","k'azont")
  sText = sText:gsub("Qu'ils ont","K'izont")
  sText = sText:gsub("Qu'elles ont","K'azont")
  sText = sText:gsub("qu'ils aient","k'izaient")
  sText = sText:gsub("qu'elles aient","k'azaient")
  sText = sText:gsub("Qu'ils aient","K'izaient")
  sText = sText:gsub("Qu'elles aient","K'azaient")
  sText = sText:gsub("Est-ce qu","Essk")
  sText = sText:gsub("est-ce qu","essk")
  sText = sText:gsub("([Pp])etit","%1'tit")
  
  -- liaisons
  sText = sText:gsub("(%s[LlDdMmCcSsTt])es ([aeiouy])","%1� z%2")
  sText = sText:gsub("(%s[LlDdMmCcSsTt])es h([aeiouy])","%1� z%2")
  sText = sText:gsub("[sx] ([ae��iouy])"," z%1")
  sText = sText:gsub("[sx] h([ae��iouy])"," z%1")
  sText = sText:gsub("(%s[JjTtMmLlSsDdNn])e(%s)","%1'")
  sText = sText:gsub("(%a)'ne(%s)","%1'n'%2")

  -- le son z
  sText = sText:gsub("([aeiouy])s([aeiouy])","%1z%2")
  
  -- les o
  sText = sText:gsub("aut(%s)","o%1")
  sText = sText:gsub("eau([- a-wyz])","o%1")
  sText = sText:gsub("au([- a-wyz])","o%1")
  
  
  -- les k
  sText = sText:gsub("c([aou])","k%1")
  sText = sText:gsub("c([LlRr])","k%1")
  sText = sText:gsub("(%a[a-mo-z])c(%s)","%1k%2")
  sText = sText:gsub("(%a)c([b-df-gj-np-tv-xz])","%1k%2")
  sText = sText:gsub("ques(%s)","ks%1")
  sText = sText:gsub("Qu","K")
  sText = sText:gsub("qu","k")
  
  -- word endings
  
  sText = sText:gsub("(%a[aeiouy])se(%s)","%1z%2")
  -- sText = sText:gsub("([ou])x(%s)","%1s%2")
  sText = sText:gsub("(%a)mme(%s)","%1m'%2")
  sText = sText:gsub("(%a)ttre(%s)","%1t'%2")
  sText = sText:gsub("(%a)er(%s)","%1�%2")
  sText = sText:gsub("(%a)ez(%s)","%1�%2")
  sText = sText:gsub("(%a)ait(%s)","%1�%2")
  sText = sText:gsub("(%a)a�t(%s)","%1�%2")
  sText = sText:gsub("(%a)ai(%s)","%1�%2")
  sText = sText:gsub("(%a)�s(%s)","%1�%2")
  sText = sText:gsub("(%a)et(%s)","%1�%2")
  sText = sText:gsub("(%a)�t(%s)","%1�%2")
  sText = sText:gsub("(%a)dre(%s)","%1d'%2")
  
  -- articles, etc.
  
  sText = sText:gsub("(%s)Il y(%s)","%1Y%2")
  sText = sText:gsub("(%s)il y(%s)","%1y%2")
  sText = sText:gsub("(%s[LlDdMmCcSsTt])es(%s)","%1�%2") 
  sText = sText:gsub("(%s)Il(%s)","%1Y%2")
  sText = sText:gsub("(%s)il(%s)","%1y%2")
  sText = sText:gsub("(%s)Ils(%s)","%1Y%2")
  sText = sText:gsub("(%s)ils(%s)","%1y%2")
  
  -- Retour � l'�tat d'origine des mots d'exception et ponctuation
  
  sText = sText:gsub("([ 'z])krmpl([ .x])","%1eau%2")
  sText = sText:gsub("([ 'z])Krmpl([ .x])","%1Eau%2")
  sText = sText:gsub("([ 'z])([Pp])krmpl([ .x])","%1%2'tet%3")
  sText = sText:gsub("((%s)[Ee])kkrmpl(%s)","%1n fait%2")
  sText = sText:gsub("((%s)[Cc])kkrmpl(%s)","%1her%2")
  sText = sText:gsub("((%s)ka(%s)","%sa%2")
  sText = sText:gsub("((%s)zkkrmpl(%s)","%en fait%2")
  
  return sText
end

-- OrcanizeIT() - applies all replacements - Italian version
function orcanizer.OrcanizeIT(sText)

  -- quick and dirty (and short) solution - thanks to runtime13
  sText = sText:gsub("s","z")
  sText = sText:gsub("q","k")
  sText = sText:gsub("c","k")
  sText = sText:gsub("zz","tz")

  return sText
end

-- OrcanizeDE() - applies all replacements - German version
function orcanizer.OrcanizeDE(sText) 
  -- specific words
  sText = sText:gsub("Zwergen(%a)","Stump'nz%1")
  sText = sText:gsub("Zwergen?","Stump'nz")
  sText = sText:gsub("Zwerginnen","Stumpinz")
  sText = sText:gsub("Zwergin","Stumpin")
  sText = sText:gsub("Zwerg","Stump'n")
  sText = sText:gsub("zwergen(%a)","stump'nz%1")
  sText = sText:gsub("zwergen?","stump'nz")
  sText = sText:gsub("zwerginnen","stumpinz")
  sText = sText:gsub("zwergin","stumpin")
  sText = sText:gsub("zwerg","stump'n")
  sText = sText:gsub("([Mm])enschen","%1�nsch'n")
  sText = sText:gsub("([Mm])ensch","%1�nsch")
  sText = sText:gsub("([Gg])oblins","%1obboz")
  sText = sText:gsub("([Gg])oblin","%1obbo")
  sText = sText:gsub("([Kk])�pfe","%1�pp�")
  sText = sText:gsub("([Kk])opf","%1opp")
  sText = sText:gsub("Leute","Gitz")
  sText = sText:gsub("Typ","Git")
  sText = sText:gsub("leute","gitz")
  sText = sText:gsub("typ","git")

  -- articles, etc.
  sText = sText:gsub("r einen","r'n")
  sText = sText:gsub("r eines","r's")
  sText = sText:gsub("(%s)[Ee]ine(%s)","%1'ne%2")
  sText = sText:gsub("(%s)[Ee]ine([nm])(%s)","%1'ne%2%3")
  sText = sText:gsub("(%s[Dd])er(%s)","%1a%2")
  sText = sText:gsub("(%s[Dd])ie(%s)","%1a%2")
  sText = sText:gsub("(%s[Dd])en(%s)","%1a%2")
  sText = sText:gsub("(%s[Dd])em(%s)","%1a%2")
  sText = sText:gsub("(%s[Dd])ass(%s)","%1az%2")
  sText = sText:gsub("(%s[Dd])as(%s)","%1at%2")
  sText = sText:gsub("(%s[Ww])as(%s)","%1at%2")

  -- word endings
  sText = sText:gsub("([A-Z���][%a'����]+[^i])es(%s)","%1�z%2")
  sText = sText:gsub("([A-Z���][%a'����]+[^i])e(%s)","%1�%2")
  sText = sText:gsub("([A-Z���][%a'����]+[^i])nen(%s)","%1n�n%2")
  sText = sText:gsub("([a-z���][%a'����]+[^i])nen(%s)","%1n'%2")
  sText = sText:gsub("([A-Z���][%a'����]+[^i])er(%s)","%1a%2")
  sText = sText:gsub("([A-Z���][%a'����]+)ers(%s)","%1az%2")
  sText = sText:gsub("([A-Z���][%a'����]+)ern(%s)","%1an%2")
  sText = sText:gsub("([%a'�������]+)ens(%s)","%1'nz%2")
  sText = sText:gsub("([%a'�������]+[^ni])en(%s)","%1'n%2")
  sText = sText:gsub("([%a'�������]+)els(%s)","%1'lz%2")
  sText = sText:gsub("([%a'�������]+[^i])el(%s)","%1'l%2")
  sText = sText:gsub("([%a'�������]+[^s�])s(%s)","%1z%2")

  -- other
  sText = sText:gsub("([%a^][%a'����]+)ist(%s)","%1is'%2")
  sText = sText:gsub("([%a^][Ss])ie(%s)","%1e%2")
  sText = sText:gsub("([%a^][Ss])ind(%s)","%1in'%2")
  sText = sText:gsub("([%a^][Uu])nd(%s)","%1n'%2")
  sText = sText:gsub("([%a^][Nn])icht(%s)","%1ich'%2")
  sText = sText:gsub("([%a^][Nn])icht[sz](%s)","%1ix%2")

  return sText  
end

function orcanizer.UpdateChatInput()
  -- save channel button's tint first (as we'll tint the whole window)
  iTintRed, iTintGreen, iTintBlue = WindowGetTintColor("EA_TextEntryGroupChannelButton")
  
  -- now update the input bar's background color
  if bUseOrcanizer then
    WindowSetTintColor("EA_TextEntryGroup",128,255,128)
  else
    WindowSetTintColor("EA_TextEntryGroup",255,255,255)
  end
  
  -- restore the channel button's tint
  WindowSetTintColor("EA_TextEntryGroupChannelButton",iTintRed,iTintGreen,iTintBlue)
end
