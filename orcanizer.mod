<?xml version="1.0" encoding="UTF-8"?>
<!-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
     http://creativecommons.org/licenses/by-nc-sa/3.0/ -->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Ingame Orcanizer" version="1.3" date="16/03/2011">
        <Author name="Tempest" email="icona@gmx.net" />
        <Description text="'Orcanizes' everything your character says as long as it's a Greenskin (Orc or Goblin).
Use /orc to enable or disable.

'Orkisiert' alles, was dein Charakter im Spiel sagt, solange er eine Grnhaut (Ork oder Goblin) ist.
Benutze /orc zum (de)aktivieren." />
    <VersionSettings gameVersion="1.4.1" />
		<Dependencies>
		</Dependencies>
        <Files>
			<File name="orcanizer.lua" />
		</Files>
        <OnInitialize>
            <CallFunction name="orcanizer.OnInitialize" />
        </OnInitialize>
		<OnUpdate/>
		<OnShutdown>
		    <CallFunction name="orcanizer.OnShutdown" />
		</OnShutdown>
		<SavedVariables>
			<SavedVariable name="bOrcanizerEnabled" />
			<SavedVariable name="bOrcanizerHardcore" />
		</SavedVariables>
		<WARInfo>
			<Categories>
				<Category name="CAREER_SPECIFIC" />
				<Category name="CHAT" />
			</Categories>
			<Careers>
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
			</Careers>
		</WARInfo>
    </UiMod>
</ModuleFile> 